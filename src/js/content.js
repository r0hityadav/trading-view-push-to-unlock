let shouldLock = false;

const isMetaKey = event => {
  const KEY = "Shift";
  const KEY1_CODE = "ShiftLeft";
  const KEY2_CODE = "ShiftRight";

  return (
    event.key === KEY && (event.code === KEY1_CODE || event.code === KEY2_CODE)
  );
};

const areAllDrawingsLocked = () => {
  const btnLockAllDrawings = document.querySelector(
    '[data-name="lockAllDrawings"]'
  );

  const LOCK_ACTIVE_CSS_CLASS = "isActive-2mI1-NUL";

  return Array.from(btnLockAllDrawings.classList).includes(
    LOCK_ACTIVE_CSS_CLASS
  );
};

const toggleLockAllDrawings = () => {
  const btnLockAllDrawings = document.querySelector(
    '[data-name="lockAllDrawings"]'
  );

  btnLockAllDrawings.click();
};

const pushToUnlockAllDrawings = e => {
  shouldLock = false;
  if (isMetaKey(e) && areAllDrawingsLocked()) {
    toggleLockAllDrawings();
  }
};

const releaseToLockAllDrawings = e => {
  shouldLock = true;
  if (isMetaKey(e) && !areAllDrawingsLocked()) {
    toggleLockAllDrawings();
  }
};

const addKeyListeners = () => {
  window.removeEventListener("keydown", pushToUnlockAllDrawings);
  window.addEventListener("keydown", pushToUnlockAllDrawings);

  window.removeEventListener("keyup", releaseToLockAllDrawings);
  window.addEventListener("keyup", releaseToLockAllDrawings);
};

const observeElement = () => {
  const btnLockAllDrawings = document.querySelector(
    '[data-name="lockAllDrawings"]'
  );

  const config = { attributes: true };
  const callback = function(mutationsList) {
    for (let mutation of mutationsList) {
      if (
        mutation.type === "attributes" &&
        !areAllDrawingsLocked() &&
        shouldLock
      ) {
        toggleLockAllDrawings();
      }
    }
  };

  const observer = new MutationObserver(callback);

  observer.observe(btnLockAllDrawings, config);
};

const init = () => {
  if (!areAllDrawingsLocked()) {
    toggleLockAllDrawings();
  }
  observeElement();
  addKeyListeners();
};

if (document.readyState !== "complete") {
  window.addEventListener("load", init);
} else {
  init();
}
